ArmorGraphicToggle = ArmorGraphicToggle or {}

local AGTenabled = false

local function print(msg)
    EA_ChatWindow.Print(towstring(msg))
end

local function ArmorGraphicToggle_SetShowHide(when, what, visibility)
    if visibility == "show" then
        AGTSettings[when][what] = true
    elseif visibility == "hide" then
        AGTSettings[when][what] = false
    end
end

local function ArmorGraphicToggle_Enable()
    AGTenabled = true
    RegisterEventHandler(SystemData.Events.LOADING_END, "ArmorGraphicToggle.SetVisibility")
    RegisterEventHandler(SystemData.Events.INTERFACE_RELOADED, "ArmorGraphicToggle.SetVisibility")
    RegisterEventHandler(SystemData.Events.PLAYER_COMBAT_FLAG_UPDATED, "ArmorGraphicToggle.SetVisibility")
end

local function ArmorGraphicToggle_Disable()
    AGTenabled = false
    UnregisterEventHandler(SystemData.Events.LOADING_END, "ArmorGraphicToggle.SetVisibility")
    UnregisterEventHandler(SystemData.Events.INTERFACE_RELOADED, "ArmorGraphicToggle.SetVisibility")
    UnregisterEventHandler(SystemData.Events.PLAYER_COMBAT_FLAG_UPDATED, "ArmorGraphicToggle.SetVisibility")
end

function ArmorGraphicToggle.SetVisibility()
    local showHelm, showCloak, showHeraldry

    if GameData.Player.inCombat then
        showHelm = AGTSettings["incombat"]["helm"]
        showCloak = AGTSettings["incombat"]["cloak"]
        showHeraldry = AGTSettings["incombat"]["heraldry"]
    else
        showHelm = AGTSettings["nocombat"]["helm"]
        showCloak = AGTSettings["nocombat"]["cloak"]
        showHeraldry = AGTSettings["nocombat"]["heraldry"]
    end

    SetEquippedItemVisible(GameData.EquipSlots.HELM, showHelm)
    SetEquippedItemVisible(GameData.EquipSlots.BACK, showCloak)

    if (GameData.Guild.m_GuildRank >= 15) and (showHeraldry ~= IsShowingCloakHeraldry()) then
        SendChatText(L"/togglecloakheraldry", L"")
	end
end

function ArmorGraphicToggle.Slash(msg)
    local opt, val = msg:match("([a-z0-9]+)[ ]?(.*)")

    if not opt then
        WindowSetShowing("AGTSettingsWindow", true)

    elseif opt == "enable" then
        if not AGTenabled then
            ArmorGraphicToggle_Enable()
            print("ArmorGraphicToggle enbled.")
        end

    elseif opt == "disable" then
        if AGTenabled then
            ArmorGraphicToggle_Disable()
            print("ArmorGraphicToggle disabled.")
        end
        
    elseif ((opt == "show") or (opt == "hide")) then
        local opt2, val2 = val:match("([a-z0-9]+)[ ]?(.*)")
        if (((opt2 == "helm") or (opt2 == "cloak") or (opt2 == "heraldry")) and ((val2 == "incombat") or (val2 == "nocombat"))) then
            ArmorGraphicToggle_SetShowHide(val2, opt2, opt)
        else
            print("Invalid values, use '/agt help' to show available commands.")
        end

    elseif opt == "help" then
        print("ArmorGraphicToggle Usage:")
        print("/agt <visibility> <what> <when>")
        print("<visibility> = show or hide")
        print("<what> = helm, cloak or heraldry")
        print("<when> = incombat or nocombat")
        print("")
        print("Example:")
        print("/agt show helm incombat")
        print("/agt hide heraldry nocombat")
    else
        print("Invalid option, use '/agt help' to show available commands.")
    end
end

function ArmorGraphicToggle.OnInitialize()
    if not AGTSettings then
        AGTSettings = {
            incombat = {
                helm = true,
                cloak = true, 
                heraldry = true,
            },
            nocombat = {
                helm = false,
                cloak = true,
                heraldry = true,
            },
        }
    end

    ArmorGraphicToggle_Enable()

    if LibSlash then
        LibSlash.RegisterSlashCmd("agt",function(msg) ArmorGraphicToggle.Slash(msg) end)
        LibSlash.RegisterSlashCmd("armorgraphic",function(msg) ArmorGraphicToggle.Slash(msg) end)
    end
    print("ArmorGraphicToggle Initialized. Configuration via /agt or /armorgraphic")
end

function ArmorGraphicToggle.OnShutdown()
    ArmorGraphicToggle_Disable()
end

AGTSettingsWindow = AGTSettingsWindow or {}

function AGTSettingsWindow.OnInitialize()
    LabelSetText("AGTSettingsWindowTitleBarText", L"AGT v1.2")
    
    LabelSetText("AGTSettingsWindowDescLabel", L"in combat/out of combat")
    
    LabelSetText("AGTSettingsWindowHelmLabel", L"Helm")
    ButtonSetCheckButtonFlag("AGTSettingsWindowHelmInCombat", true)
    ButtonSetCheckButtonFlag("AGTSettingsWindowHelmNoCombat", true)

    LabelSetText("AGTSettingsWindowCloakLabel", L"Cloak")
    ButtonSetCheckButtonFlag("AGTSettingsWindowCloakInCombat", true)
    ButtonSetCheckButtonFlag("AGTSettingsWindowCloakNoCombat", true)

    LabelSetText("AGTSettingsWindowHeraldryLabel", L"Heraldry")
    ButtonSetCheckButtonFlag("AGTSettingsWindowHeraldryInCombat", true)
    ButtonSetCheckButtonFlag("AGTSettingsWindowHeraldryNoCombat", true)
end

function AGTSettingsWindow.FillSettingsWindow()
    ButtonSetPressedFlag("AGTSettingsWindowHelmInCombat", AGTSettings["incombat"]["helm"])
    ButtonSetPressedFlag("AGTSettingsWindowHelmNoCombat", AGTSettings["nocombat"]["helm"])
    ButtonSetPressedFlag("AGTSettingsWindowCloakInCombat", AGTSettings["incombat"]["cloak"])
    ButtonSetPressedFlag("AGTSettingsWindowCloakNoCombat", AGTSettings["nocombat"]["cloak"])
    ButtonSetPressedFlag("AGTSettingsWindowHeraldryInCombat", AGTSettings["incombat"]["heraldry"])
    ButtonSetPressedFlag("AGTSettingsWindowHeraldryNoCombat", AGTSettings["nocombat"]["heraldry"])

    if GameData.Guild.m_GuildRank < 15 then
        ButtonSetDisabledFlag("AGTSettingsWindowHeraldryInCombat", true)
        ButtonSetDisabledFlag("AGTSettingsWindowHeraldryNoCombat", true)
    else
        ButtonSetDisabledFlag("AGTSettingsWindowHeraldryInCombat", false)
        ButtonSetDisabledFlag("AGTSettingsWindowHeraldryNoCombat", false)
    end
end

function AGTSettingsWindow.ApplySettings()
    AGTSettings = { 
        incombat = {
            helm = ButtonGetPressedFlag("AGTSettingsWindowHelmInCombat"),
            cloak = ButtonGetPressedFlag("AGTSettingsWindowCloakInCombat"),
            heraldry = ButtonGetPressedFlag("AGTSettingsWindowHeraldryInCombat"),
        },
        nocombat = {
            helm = ButtonGetPressedFlag("AGTSettingsWindowHelmNoCombat"),
            cloak = ButtonGetPressedFlag("AGTSettingsWindowCloakNoCombat"),
            heraldry = ButtonGetPressedFlag("AGTSettingsWindowHeraldryNoCombat"),
        },
    }
    ArmorGraphicToggle.SetVisibility()
end

function AGTSettingsWindow.CloseSettingsWindow()
    WindowSetShowing("AGTSettingsWindow", false)
end

<?xml version="1.0" encoding="UTF-8"?>
<ModuleFile xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
    <UiMod name="ArmorGraphicToggle" version="1.2" date="08/23/2010" >
        <VersionSettings gameVersion="1.3.6" windowsVersion="1.0" savedVariablesVersion="1.0" />
        <Author name="Garkin" email="garkin@bigboss.cz" />
        <Description text="Automaticaly toggles on/off armor graphic whenever you enter/leave combat." />

        <WARInfo>
            <Categories>
                <Category name="COMBAT" />
                <Category name="OTHER" />
            </Categories>
            <Careers>
                <Career name="BLACKGUARD" />
                <Career name="WITCH_ELF" />
                <Career name="DISCIPLE" />
                <Career name="SORCERER" />
                <Career name="IRON_BREAKER" />
                <Career name="SLAYER" />
                <Career name="RUNE_PRIEST" />
                <Career name="ENGINEER" />
                <Career name="BLACK_ORC" />
                <Career name="CHOPPA" />
                <Career name="SHAMAN" />
                <Career name="SQUIG_HERDER" />
                <Career name="WITCH_HUNTER" />
                <Career name="KNIGHT" />
                <Career name="BRIGHT_WIZARD" />
                <Career name="WARRIOR_PRIEST" />
                <Career name="CHOSEN" />
                <Career name="MARAUDER" />
                <Career name="ZEALOT" />
                <Career name="MAGUS" />
                <Career name="SWORDMASTER" />
                <Career name="SHADOW_WARRIOR" />
                <Career name="WHITE_LION" />
                <Career name="ARCHMAGE" />
            </Careers>
        </WARInfo>

        <Dependencies>
            <Dependency name="EA_CharacterWindow" />
            <Dependency name="EA_ChatWindow" />
            <Dependency name="LibSlash" />
        </Dependencies>

        <SavedVariables>
            <SavedVariable name="AGTSettings" />
        </SavedVariables>

        <Files>
            <File name="ArmorGraphicToggle.lua" />
            <File name="AGTSettingsWindow.xml" />
        </Files>

        <OnInitialize>
            <CallFunction name="ArmorGraphicToggle.OnInitialize" />
            <CreateWindow name="AGTSettingsWindow" show="false" />
            <CallFunction name="AGTSettingsWindow.OnInitialize" />
        </OnInitialize>
        
        <OnShutdown>
            <CallFunction name="ArmorGraphicToggle.OnShutdown" />
        </OnShutdown>
        
    </UiMod>
</ModuleFile>
